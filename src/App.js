import React from 'react'
import './App.css'
import Board from './components/Board'
import Cemetery from './components/Cemetery'
import Hand from './components/Hand'
import Players from './components/Players'
import { StateProvider } from './state'

function App() {
    return (
        <StateProvider>
            <Players />
            <Board />
            <Hand />
            <Cemetery />
        </StateProvider>
    )
}

export default App
