import React from 'react'
import styled from 'styled-components'

const StyledCard = styled.div`
    width: 100px;
    height: 120px;
    background-color: red;
    margin: 0px 10px;
`

const Card = (props) => {
    const { children, ...rest } = props

    return <StyledCard {...rest}>{children}</StyledCard>
}

export default Card
