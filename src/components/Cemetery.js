import React from 'react'
import Card from './Card'
import styled from 'styled-components'
import { useGame } from '../state'

const StyledCemetery = styled.div`
    position: fixed;
    bottom: 0px;
    left: 0px;
`

const Cemetery = () => {
    const [state, dispatch] = useGame()

    return (
        <StyledCemetery>
            <Card>{state.cemetery.length} cartes</Card>
        </StyledCemetery>
    )
}

export default Cemetery
