import React from 'react'
import styled from 'styled-components'
import { ACTION, useGame } from '../state'
import Card from './Card'

const StyledBoard = styled.div`
    display: flex;
    border: 1px solid grey;
    height: 120px;
    justify-content: center;
    margin: 0 auto;
    margin-top: 25vh;
    padding: 50px 0;
    width: 800px;
`

const Board = () => {
    const [state, dispatch] = useGame()

    const doSomething = () => {
        dispatch({ type: ACTION.ADD_CART_TO_CEMETERY, card: '2' })
    }

    return (
        <StyledBoard>
            {state.board.map((card, id) => (
                <Card key={id} />
            ))}
        </StyledBoard>
    )
}

export default Board
