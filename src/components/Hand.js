import React from 'react'
import Card from './Card'
import styled from 'styled-components'
import { ACTION, useGame } from '../state'

const StyledHand = styled.div`
    position: fixed;
    bottom: 0px;
    margin-left: 25%;
    display: flex;
    flex-direction: row;
`

const Hand = () => {
    const [state, dispatch] = useGame()

    const doSomething = () => {
        dispatch({ type: ACTION.ADD_CART_TO_BOARD, card: '2' })
    }

    return (
        <StyledHand>
            {state.hand.map((card, id) => (
                <Card key={id} onClick={() => doSomething()} />
            ))}
        </StyledHand>
    )
}

export default Hand
