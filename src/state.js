import React from 'react'

const initialState = {
    board: [],
    hand: ['1', '1', '1'],
    cemetery: [],
}

const ACTION = {
    ADD_CART_TO_BOARD: 'ADD_CART_TO_BOARD',
    ADD_CART_TO_CEMETERY: 'ADD_CART_TO_CEMETERY',
}

const reducer = (state, action) => {
    console.log(state, action)
    switch (action.type) {
        case ACTION.ADD_CART_TO_BOARD:
            return {
                ...state,
                board: [...state.board, action.card],
                hand: state.hand.filter((card, id) => id !== 0),
            }
        case ACTION.ADD_CART_TO_CEMETERY:
            return {
                ...state,
                board: state.board.filter((card, id) => id !== 0),
                cemetery: [...state.cemetery, action.card],
            }
        default: {
           // throw new Error(`Unhandled action type: ${action.type}`)
        }
    }
}

const StateContext = React.createContext()
const StateDispatchContext = React.createContext()

function StateProvider({ children }) {
    const [state, dispatch] = React.useReducer(reducer, initialState)
    return (
        <StateContext.Provider value={state}>
            <StateDispatchContext.Provider value={dispatch}>
                {children}
            </StateDispatchContext.Provider>
        </StateContext.Provider>
    )
}

function useState() {
    const context = React.useContext(StateContext)
    if (context === undefined) {
        throw new Error('useCountState must be used within a CountProvider')
    }
    return context
}

function useDispatch() {
    const context = React.useContext(StateDispatchContext)
    if (context === undefined) {
        throw new Error('useCountDispatch must be used within a CountProvider')
    }
    return context
}

function useGame() {
    return [useState(), useDispatch()]
}

export { useGame, StateProvider, ACTION }
